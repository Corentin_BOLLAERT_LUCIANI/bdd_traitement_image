import java.util.*;
import java.io.*;
import java.util.stream.Collectors;

/**
 *
 * @author Corentin BOLLAERT-LUCIANI
 */
public class QBE {

    public static void main(String[] args) {
        if(args.length < 3){
            System.out.println("Nombre insuffisant de parametres");
            return;
        }else if (args.length > 3) {
            System.out.println("Trop de parametres");
            return;
        }
        else{
            System.out.println("Fichier contenant le descripteur : "+ args[0]);
            System.out.println("Image de la requete : "+ args[1]);
            System.out.println("nombre de resultat : "+ args[2]);
        }
        recherche(args[0], args[1], Integer.parseInt(args[2]));
    }

    static void recherche(String descriptor, String image_name, int number_of_results){
        
        //Chemin relatif du fichier en fonction du nom en entré
        String descriptor_filename = "./Histogrammes/"+descriptor+".txt";
        
        // Map stockant les scores des images en fonction de leurs noms
        Map<String,Double> scoreMap = new HashMap<String,Double>();

        BufferedReader reader;
        
        String ParsedTestedImageLine = "";
        String[] DescriptorValue = null;
        try{
            reader = new BufferedReader(new FileReader(descriptor_filename));
            String line = reader.readLine();

            // Je recherche la ligne de l'image désirée;
            while (line != null) {

                // Quand la ligne correspond à l'image recherchée...
                if (line.split(":")[0].equals(image_name)) {
                    
                    ParsedTestedImageLine = line.split(":")[1];
                    
                    ParsedTestedImageLine = ParsedTestedImageLine
                    .replace(" ", "")
                    .replace("][", ",")
                    .replace("[", "")
                    .replace("]", "");

                    DescriptorValue = ParsedTestedImageLine.split(",");

                    break;
                }

                line = reader.readLine();
			}
            reader.close();

            // Si l'image n'est pas présente, je retourne une erreur
            if (ParsedTestedImageLine.isEmpty()) throw new Error("L'image demandee n'est pas presente");

			reader = new BufferedReader(new FileReader(descriptor_filename));
            line = reader.readLine();

            // Nom de l'image en train d'être traitée
            String currentImageName = "";
            // Score de correspondance
            double currentImageScore = 0;

            // Contenue du descripteur de l'image analysée
            String CurrentImageDescriptorString = "";
            String[] DescriptorValueArray = null;

			while (line != null) {
                String[] splitedLine = line.split(":");
                currentImageName = splitedLine[0];
                // initialise le score
                currentImageScore = 0;

                // Je formate
                CurrentImageDescriptorString = splitedLine[1]
                .replace(" ", "")
                .replace("][", ",")
                .replace("[", "")
                .replace("]", "");

                DescriptorValueArray = CurrentImageDescriptorString.split(",");

                if (DescriptorValue.length != DescriptorValueArray.length ) {
                    throw new Error("Les descripteurs ne sont pas de le même taille, une erreur s'est produite pendant l'indexation");
                }

                for (int i = 0; i < DescriptorValue.length; i++){
                    currentImageScore += Math.pow(Float.parseFloat(DescriptorValue[i]) - Float.parseFloat(DescriptorValueArray[i]),2);
                }
                // Ajout de la racine carré du score
                scoreMap.put(currentImageName, Math.pow(currentImageScore,1.0/2));

				line = reader.readLine();
			}
			reader.close();

            String fileNameOuput = "./Resultats/recherche_par_similarite_"+image_name+"_"+descriptor+".html";

            // Création d'une map qui seras ordonnée selon le score de correspondance
            Map<String,Double> sortedMap = scoreMap.entrySet().
                        stream().
                        sorted(Map.Entry.comparingByValue()).
            collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
            

            // Ecriture du fichier
            TextFileWriting(fileNameOuput, false, "<html>");
            TextFileWriting(fileNameOuput, true, "<title>recherche_par_similarite_"+image_name+"_"+descriptor+"</title>");

            // Chaque ligne est écrite dans le fichier
            int ResultCount = 1;
            for(String imageNameKey : sortedMap.keySet()) {
                TextFileWriting(fileNameOuput, true, "<img src=\"../Base10000/images/"+imageNameKey+"\" width=\"100\" title=\""+imageNameKey+"\">");
                if (number_of_results != 0) {
                    ResultCount ++;
                    if (ResultCount > number_of_results) {
                        break;
                    }
                }
            }

            TextFileWriting(fileNameOuput, true, "</html>");

            System.out.println("Termine");

		} catch (IOException e) {
			e.printStackTrace();
		}
    }

    static void TextFileWriting(String fileName, Boolean append, String text) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, append));
            writer.append(text);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

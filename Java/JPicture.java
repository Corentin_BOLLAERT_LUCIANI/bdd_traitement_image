import java.awt.image.*;
import javax.imageio.ImageIO;
import java.util.*;
import java.io.*;


/**
 *
 * @author Marin Ferecatu
 */
public class JPicture {
    static final int RGB_DEPTH = 3, depth = 3, RED = 0, GREEN = 1, BLUE = 2;
    String filename;
    boolean readError;
    int width, height, type;
    float[][][] pixels;

    public JPicture() {
    }

    public JPicture(String fileName) {
        readImageFile(fileName);
    }

    public JPicture(int width, int height, int type, String filename, float[][][] pixels) {
        // deep constructor
        this.width = width;
        this.height = height;
        this.type = type;
        this.filename = filename;
        float[][][] pc = clonePixels(pixels);
        this.pixels = pc;
    }

    public JPicture(int width, int height) {
        this.width = width;
        this.height = height;
        type = 5; // BufferedImage.TYPE_3BYTE_BGR
        pixels = new float[width][height][RGB_DEPTH];
    }

    double max3(double a, double b, double c) {
        return ((a > b) ? ((a > c) ? a : c) : ((b > c) ? b : c));
    }

    double min3(double a, double b, double c) {
        return ((a < b) ? ((a < c) ? a : c) : ((b < c) ? b : c));
    }

    float[][][] clonePixels(float[][][] pixels) {
        int x, y;
        float[][][] pc = new float[width][height][RGB_DEPTH]; // pixels copy
        for(x = 0; x < width; x++) for(y = 0; y < height; y++) {
            System.arraycopy(pixels[x][y], 0, pc[x][y], 0, RGB_DEPTH);
        }
        return pc;
    }

    void setPixels(double[][] v) {
        width = v.length;
        height = v[0].length;
        pixels = new float[width][height][RGB_DEPTH];
        for(int x = 0; x < width; x++) for(int y = 0; y < height; y++) {
            pixels[x][y][RED] = pixels[x][y][GREEN] = pixels[x][y][BLUE] = (float)v[x][y];
        }
    }
    
    void readImageFile(String fileName){
        try {
            BufferedImage img = ImageIO.read(new File(fileName));
            width = img.getWidth();
            height = img.getHeight();
            // type = img.getType();
            type = 5; // BufferedImage.TYPE_3BYTE_BGR
            pixels = new float[width][height][RGB_DEPTH];
            int[] rgbArray = img.getRGB(0, 0, width, height, null, 0, width);
            int pixel;
            for(int x = 0; x < width; ++x)
                for(int y = 0; y < height; ++y) {
                    pixel = rgbArray[y*width + x];
                    // int alpha = (pixel >> 24) & 0xff; // we do not use alpha
                    pixels[x][y][RED]   = (float)((pixel >> 16) & 0xff)/255.0f;
                    if(pixels[x][y][RED] == 1) pixels[x][y][RED] -= 1e-5f;
                    pixels[x][y][GREEN] = (float)((pixel >>  8) & 0xff)/255.0f;
                    if(pixels[x][y][GREEN] == 1) pixels[x][y][GREEN] -= 1e-5f;
                    pixels[x][y][BLUE]  = (float)((pixel      ) & 0xff)/255.0f;
                    if(pixels[x][y][BLUE] == 1) pixels[x][y][BLUE] -= 1e-5f;
                }
        } catch (Exception e) {
            e.printStackTrace();
            readError = true;
        }
    }

    void writeImageFile(String filename) {
        int[] rgbArray = new int[width*height];
        this.filename = filename;
        for(int x = 0; x < width; ++x)
            for(int y = 0; y < height; ++y) {
                rgbArray[y*width + x] = (((int)(pixels[x][y][RED]*255.0f) & 0xff)   << 16) |
                                        (((int)(pixels[x][y][GREEN]*255.0f) & 0xff) <<  8) |
                                        (((int)(pixels[x][y][BLUE]*255.0f) & 0xff)       );
            }
        BufferedImage img = new BufferedImage(width, height, type);
        img.setRGB(0, 0, width, height, rgbArray, 0, width);
        try {
            ImageIO.write(img, "jpg", new File(filename));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void TextFileWriting(String fileName, Boolean append, String text) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, append));
            writer.append(text);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // // RGB to GBR
    // public static void main(String[] args) {
    //     JPicture p = new JPicture("horse.jpg");

    //     for(int i = 0; i < p.width; ++i) {
    //         for(int j = 0; j < p.height; ++j) {
    //             p.pixels[i][j][RED] = p.pixels[i][j][GREEN];
    //             p.pixels[i][j][GREEN] = p.pixels[i][j][BLUE];
    //             p.pixels[i][j][BLUE] =  p.pixels[i][j][RED];
    //         }
    //     }
    //     p.writeImageFile("horse1.jpg");
    // }

    //Damier
    // public static void main(String[] args) {
    //     JPicture p = new JPicture(400,400);

    //     for(int i = 0; i < 400; ++i) {
    //         for(int j = 0; j < 400; ++j) {
    //             p.pixels[i][j][RED] = (i+j)%2==0 ? 0 : 1;
    //             p.pixels[i][j][GREEN] = (i+j)%2==0 ? 1 : 0;
    //             p.pixels[i][j][BLUE] =  0;
    //         }
    //     }

    //     p.writeImageFile("damier.jpg");
    // }



    public static double Luminence(double RED, double GREEN, double BLUE){
        return 0.299 * RED + 0.587 * GREEN + 0.114 * BLUE;
    }

    public static float[] ComputeGrayLevelHistogram(int nbBins, String foldername, String fileName){
        String filePath = foldername+"/"+fileName;
        JPicture p = new JPicture(filePath);
        float[] histogramme = new float[nbBins];

        float plage = 1.0f / nbBins;

        for(int i = 0; i < p.width; ++i) {
            for(int j = 0; j < p.height; ++j) {
                double lum = Luminence(p.pixels[i][j][RED], p.pixels[i][j][GREEN], p.pixels[i][j][BLUE]);
                histogramme[(int)Math.floor(lum / plage)] ++;
            }
        }

        int picture_MN = p.height * p.width;
        
        for(int i = 0; i < histogramme.length  ; ++i)  {
            histogramme[i] /= picture_MN;
        }

        String filenameOutput = "./Histogrammes/GrayHistogram"+nbBins;
        String extension = ".txt";
        String fileOutput = filenameOutput + extension;

        TextFileWriting(fileOutput, true,  fileName+":");

        TextFileWriting(fileOutput, true,  Arrays.toString(histogramme));
        
        TextFileWriting(fileOutput, true, System.lineSeparator());
        
        return histogramme;
    }

    public static float[][][] ComputeRGBHistogram(int nbBins, String foldername, String fileName){ 
        String filePath = foldername+"/"+fileName;
        JPicture p = new JPicture(filePath);
        
        float[][][] histogramme = new float[nbBins][nbBins][nbBins];

        float plage = 1.0f / nbBins;

        int picture_MN = p.height * p.width;

        for(int i = 0; i < p.width; ++i) {
            for(int j = 0; j < p.height; ++j) {
                histogramme
                [(int)Math.floor(p.pixels[i][j][RED] / plage)]
                [(int)Math.floor(p.pixels[i][j][GREEN] / plage)]
                [(int)Math.floor(p.pixels[i][j][BLUE] / plage)]
                += 1.0f / picture_MN;
                // ++;
            }
        }

        // System.out.println("[R0 + G0 + B0 ||| R0 + G0 + B1]");
        // System.out.println("[R0 + G1 + B0 ||| R0 + G1 + B1]");
        // System.out.println("");
        // System.out.println("[R1 + G0 + B0 ||| R1 + G0 + B1]");
        // System.out.println("[R1 + G1 + B0 ||| R1 + G1 + B1]");
        
        String filenameOutput = "./Histogrammes/RGBHistogram"+nbBins+"x"+nbBins+"x"+nbBins;
        String extension = ".txt";
        String fileOutput = filenameOutput + extension;

        TextFileWriting(fileOutput, true,  fileName+":");

        for(int red = 0; red < nbBins; ++red) {
            for(int green = 0; green < nbBins; ++green) {
                TextFileWriting(fileOutput, true,  Arrays.toString(histogramme[red][green]));
            }
            TextFileWriting(fileOutput, true, " ");
        }
        
            TextFileWriting(fileOutput, true, System.lineSeparator());
        return histogramme;
    
    }

    public static void IndexDatabaseRGB(int nbBrins){

        String relativeImagesFolderPath = "./Base10000/images";
        File folder = new File(relativeImagesFolderPath);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                ComputeRGBHistogram(nbBrins, relativeImagesFolderPath, listOfFiles[i].getName());
            }
        }
    }

    public static void IndexDatabaseGray(){

        String relativeImagesFolderPath = "./Base10000/images";
        File folder = new File(relativeImagesFolderPath);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                ComputeGrayLevelHistogram(256, relativeImagesFolderPath, listOfFiles[i].getName());
            }
        }
    }
    public static void IndexEverything(){
        IndexDatabaseRGB(2);
        IndexDatabaseRGB(4);
        IndexDatabaseRGB(6);
        IndexDatabaseGray();
    }

    public static void main(String[] args) {
        IndexEverything();
    }
}
